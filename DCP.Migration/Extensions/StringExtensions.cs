﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;

namespace DCP.Migration.Extensions
{
    public static class StringExtensions
    {
        private const int VectorSize = 16;

        public static string Encrypt(this string input, string passPhrase)
        {
            var objInitVector = passPhrase.Substring(0, VectorSize);
            var objInitVectorBytes = Encoding.UTF8.GetBytes(objInitVector);
            var objPlainTextBytes = Encoding.UTF8.GetBytes(input);
            var objPassword = new PasswordDeriveBytes(passPhrase, null);
            var objKeyBytes = objPassword.GetBytes(256 / 8);
            var objSymmetricKey = new RijndaelManaged
            {
                Mode = CipherMode.CBC
            };
            var objEncryptor = objSymmetricKey.CreateEncryptor(objKeyBytes, objInitVectorBytes);
            var objMemoryStream = new MemoryStream();
            var objCryptoStream = new CryptoStream(objMemoryStream, objEncryptor, CryptoStreamMode.Write);
            objCryptoStream.Write(objPlainTextBytes, 0, objPlainTextBytes.Length);
            objCryptoStream.FlushFinalBlock();
            var objEncrypted = objMemoryStream.ToArray();
            objMemoryStream.Close();
            objCryptoStream.Close();
            return Convert.ToBase64String(objEncrypted);
        }
    }
}
