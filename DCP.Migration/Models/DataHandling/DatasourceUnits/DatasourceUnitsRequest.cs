﻿using DCP.Migration.Models.Site;

namespace DCP.Migration.Models.DataHandling.DatasourceUnits
{
    public class DatasourceUnitsRequest
    {
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
