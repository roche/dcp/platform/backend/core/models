﻿namespace DCP.Migration.Models.DataHandling.DatasourceUnits
{
    public class DatasourceUnitResponseDTO
    {
        public string UoMClass { get; set; }
        public string UoMName { get; set; }
        public string UoMAbbreviation { get; set; }
    }
}
