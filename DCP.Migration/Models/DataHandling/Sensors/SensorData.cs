﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SensorData
    {
        public string Name { get; set; }
        public string WebId { get; set; }
        public string UoM { get; set; }
        //public List<XYDTO> XYData { get; set; }

        public List<string> XValues { get; set; }

        public List<string> YValues { get; set; }

        public int BadCount { get; set; }

        public int TotalCount { get; set; }
        public JObject FormulaSensors { get; set; }
    }
}
