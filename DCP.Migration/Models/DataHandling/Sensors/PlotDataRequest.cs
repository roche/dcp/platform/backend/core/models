﻿using DCP.Migration.Models.Site;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class PlotDataRequest
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public int Intervals { get; set; }
        public List<string> SensorsWebIds { get; set; }
        public bool Refresh { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
        public string BatchStartTime { get; set; }
    }
}
