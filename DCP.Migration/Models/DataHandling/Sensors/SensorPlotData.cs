﻿using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SensorPlotData
    {
        public string SensorName { get; set; }
        public string SensorDescription { get; set; }
        public string DeviceName { get; set; }
        public string WebId { get; set; }
        public string UoM { get; set; }
        public List<DataPointStringDTO> DataPoints { get; set; }

        public int BadCount { get; set; }

        public int TotalCount { get; set; }
    }
}
