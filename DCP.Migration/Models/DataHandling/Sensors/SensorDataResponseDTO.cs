﻿using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SensorDataResponseDTO
    {
        public List<SensorItemDTO> Items { get; set; }
    }
}
