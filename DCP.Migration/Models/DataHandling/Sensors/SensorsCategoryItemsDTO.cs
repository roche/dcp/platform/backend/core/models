﻿using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SensorsCategoryItemsDTO
    {
        public string CategoryName { get; set; }
        public List<SensorIdentifierDTO> SensorIdentifiers { get; set; }
    }
}
