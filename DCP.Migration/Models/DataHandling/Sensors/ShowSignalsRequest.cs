﻿using DCP.Migration.Models.OsiSoftPI;
using DCP.Migration.Models.Site;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class ShowSignalsRequest : PIFilter
    {
        public List<string> SensorsWebIds { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
        public string WebIdOfMaturity { get; set; }
    }
}
