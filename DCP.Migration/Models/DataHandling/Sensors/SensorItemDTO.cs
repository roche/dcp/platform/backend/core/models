﻿using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SensorItemDTO
    {
        public string WebId { get; set; }
        public List<SensorItemItemDTO> Items { get; set; }
    }
}
