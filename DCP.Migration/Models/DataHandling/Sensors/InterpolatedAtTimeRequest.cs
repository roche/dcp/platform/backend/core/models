﻿using DCP.Migration.Models.Site;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class InterpolatedAtTimeRequest
    {
        public List<string> SensorsWebIds { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
        public List<string> Times { get; set; }
    }
}
