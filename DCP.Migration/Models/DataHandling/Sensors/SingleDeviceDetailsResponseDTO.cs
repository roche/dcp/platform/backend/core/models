﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SingleDeviceDetailsResponseDTO
    {
        public string WebId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public bool HasChildren { get; set; }
        public List<string> CategoryNames { get; set; }
        public JObject Links { get; set; }
    }
}