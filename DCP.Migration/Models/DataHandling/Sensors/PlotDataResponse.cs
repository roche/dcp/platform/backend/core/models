﻿using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class PlotDataResponse
    {
        public List<SensorPlotData> SensorPlotData { get; set; }
        public byte WarningCode { get; set; }
    }
}
