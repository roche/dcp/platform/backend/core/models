﻿namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class XYValues
    {
        public string TimeStamp { get; set; }
        public string Value { get; set; }
        public string Good { get; set; }
    }
}
