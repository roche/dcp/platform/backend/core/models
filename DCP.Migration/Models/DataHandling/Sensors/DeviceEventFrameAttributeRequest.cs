﻿using DCP.Migration.Models.Site;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class DeviceEventFrameAttributeRequest
    {
        public string DeviceWebId { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
