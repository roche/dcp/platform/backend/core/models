﻿using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class ShowSignalsResult
    {
        public List<DataPointStringDTO> DataPoints { get; set; }
        public int FirstItemTotalSeconds { get; set; }
        public string UnitOfMeasureXAxis { get; set; }
        public string UnitOfMeasureYAxis { get; set; }
        public byte WarningCode { get; set; }
    }
}
