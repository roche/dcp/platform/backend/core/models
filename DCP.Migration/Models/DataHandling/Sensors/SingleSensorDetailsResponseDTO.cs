﻿using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SingleSensorDetailsResponseDTO
    {
        public string WebId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
        public string DefaultUnitsNameAbbreviation { get; set; }
        public List<string> CategoryNames { get; set; }
        public bool Step { get; set; }
        public List<string> Links { get; set; }
    }
}