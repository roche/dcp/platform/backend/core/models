﻿namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SensorIdentifierDTO
    {
        public string Name { get; set; }
        public string WebId { get; set; }

        public string Description { get; set; }
        public string ValueType { get; set; }
        public bool IsStep { get; set; }
    }
}
