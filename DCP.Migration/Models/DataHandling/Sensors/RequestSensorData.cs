﻿using DCP.Migration.Models.OsiSoftPI;
using DCP.Migration.Models.Site;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class RequestSensorData : PIFilter
    {
        /// <summary>
        /// Sensors web ids 
        /// </summary>
        public List<string> WebIds { get; set; }
        public bool Refresh { get; set; }
        public List<string> DeviceWebIds { get; set; }
        /// <summary>
        /// Sensors - 1 Equation -2
        /// </summary>
        public byte SensorCalculationType { get; set; }
        public List<string> Expressions { get; set; }
        public Dictionary<string, string> SensorsWebIdsPerDeviceWebid { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
