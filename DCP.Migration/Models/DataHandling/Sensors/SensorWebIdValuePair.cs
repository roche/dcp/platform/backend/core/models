﻿namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SensorWebIdValuePair
    {
        public string SensorWebId { get; set; }
        public double Value { get; set; }
    }
}
