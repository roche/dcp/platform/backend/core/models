﻿using DCP.Migration.Models.Site;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class MultipleSensorDetailsRequest
    {
        public List<string> SensorsWebIds { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
