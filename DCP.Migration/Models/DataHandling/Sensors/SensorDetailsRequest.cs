﻿using DCP.Migration.Models.Site;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SensorDetailsRequest
    {
        public string SensorWebId { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
