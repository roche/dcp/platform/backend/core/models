﻿namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class SensorItemItemDTO
    {
        /// <summary>
        /// Can be double or string
        /// </summary>
        public dynamic Value { get; set; }
        public bool Good { get; set; }
        public string Timestamp { get; set; }
    }
}
