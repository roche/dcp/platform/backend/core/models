﻿namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class DataPointStringPerSensorWebIdDTO : DataPointStringDTO
    {
        public string SensorWebId { get; set; }
    }
}
