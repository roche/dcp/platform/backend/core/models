﻿using System;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class DataPointStringDTO
    {
        public string Value { get; set; }
        public double? Interval { get; set; }
        public DateTime ObsID { get; set; }

        public bool BadValue { get; set; }

        /// <summary>
        /// NegativeInfinity = 1, PositiveInfinity = 2, NaN = 3 - If some of those values are detected Value property will be set to NULL
        /// </summary>
        public byte? ValueSpecialValueType { get; set; }
        /// <summary>
        /// NegativeInfinity = 1, PositiveInfinity = 2, NaN = 3 - If some of those values are detected Interval property will be set to NULL
        /// </summary>
        public byte? IntervalSpecialValueType { get; set; }
    }
}
