﻿using System;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class DataPointDTO
    {
        public double? Value { get; set; }
        public double? Interval { get; set; }
        public DateTime ObsID { get; set; }
        public bool BadValue { get; set; }
    }
}
