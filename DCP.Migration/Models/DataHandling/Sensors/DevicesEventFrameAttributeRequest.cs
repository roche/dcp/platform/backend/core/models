﻿using DCP.Migration.Models.Site;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class DevicesEventFrameAttributeRequest
    {
        public List<string> DeviceWebIds { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
