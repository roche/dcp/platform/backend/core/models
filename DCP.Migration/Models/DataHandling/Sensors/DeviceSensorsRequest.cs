﻿using DCP.Migration.Models.Site;

namespace DCP.Migration.Models.DataHandling.Sensors
{
    public class DeviceSensorsRequest
    {
        public string DeviceWebId { get; set; }
        public string GroupType { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
