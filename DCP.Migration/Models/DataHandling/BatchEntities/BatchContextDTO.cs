﻿using DCP.Migration.Models.DataHandling.Sensors;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.BatchEntities
{
    public class BatchContextDTO
    {
        public JArray BatchContext { get; set; }
        public List<SensorData> SensorData { get; set; }
        public string DeviceWebId { get; set; }
        public byte WarningCode { get; set; }
    }
}
