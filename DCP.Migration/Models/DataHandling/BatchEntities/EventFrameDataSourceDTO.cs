﻿using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.BatchEntities
{
    public class EventFrameDataSourceDTO
    {
        public string EventFrameId { get; set; }
        public List<EventFrameAttributeValueDTO> AttributeValues { get; set; }
    }
}
