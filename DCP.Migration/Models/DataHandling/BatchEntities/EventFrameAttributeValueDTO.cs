﻿namespace DCP.Migration.Models.DataHandling.BatchEntities
{
    public class EventFrameAttributeValueDTO
    {
        public string Name { get; set; }
        public dynamic Value { get; set; }
        public bool Good { get; set; }
    }
}
