﻿namespace DCP.Migration.Models.DataHandling.BatchEntities
{
    public class DeviceEventFrameAttributeDTO
    {
        public string DeviceWebId { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public string Type { get; set; }
    }
}
