﻿using DCP.Migration.Models.DataHandling.BatchResponses;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.BatchEntities
{
    public class BatchMetadataInformation
    {
        public List<BatchMetadataInformationResponse> Data { get; set; }
        public JObject DataTypes { get; set; }
        /// <summary>
        /// Check WarningStatusCodes enum
        /// </summary>
        public int WarningCode { get; set; }
    }
}
