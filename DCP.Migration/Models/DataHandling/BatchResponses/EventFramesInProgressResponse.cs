﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.BatchResponses
{
    public class EventFramesInProgressResponse
    {
        public bool IsSuccessStatusCode { get; set; }
        public JObject Response { get; set; }
        public Dictionary<string, string> IndexesToDeviceWebIds { get; set; }
    }
}
