﻿using DCP.Migration.Models.OsiSoftPI;
using Newtonsoft.Json.Linq;

namespace DCP.Migration.Models.DataHandling.BatchResponses
{
    public class BatchMetadataInformationResponse : PIFilter
    {
        //public string Item { get; set; }
        //public string BatchId { get; set; }

        public string Id { get; set; }
        public string DeviceWebId { get; set; }
        public string DeviceName { get; set; }

        public JObject Values { get; set; }
        public double IntervalInSeconds { get; set; }

    }
}
