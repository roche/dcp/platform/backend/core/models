﻿using DCP.Migration.Models.DataHandling.BatchEntities;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.BatchResponses
{
    public class EventFramesAttributeValuesDataSourceResponse
    {
        public List<EventFrameDataSourceDTO> EventFrames;
        public byte WarningCode { get; set; }
    }
}
