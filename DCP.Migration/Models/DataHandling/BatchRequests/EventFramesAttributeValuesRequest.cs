﻿using DCP.Migration.Models.Site;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.BatchRequests
{
    public class EventFramesAttributeValuesRequest
    {
        public List<string> EventFrameIds { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
