﻿using DCP.Migration.Models.Site;

namespace DCP.Migration.Models.DataHandling.BatchRequests
{
    public class EventFrameEndTimeRequest
    {
        public string EventFrameId { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
