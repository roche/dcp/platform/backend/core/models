﻿using DCP.Migration.Models.Site;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.BatchRequests
{
    public class DevicesEventFramesRequest
    {
        public List<string> DeviceWebIds { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
