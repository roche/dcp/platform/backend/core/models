﻿using DCP.Migration.Models.Site;

namespace DCP.Migration.Models.DataHandling.BatchRequests
{
    public class DeviceEventFramesRequest
    {
        public string DeviceWebId { get; set; }
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public bool IsActive { get; set; }
        public bool IsEndTimeOverwritten { get; set; }
        public int ModelId { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
