﻿using DCP.Migration.Models.Site;
using System.Collections.Generic;

namespace DCP.Migration.Models.DataHandling.BatchRequests
{
    public class EventFramesInProgressRequest
    {
        public List<string> DeviceWebIds { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
