﻿using DCP.Migration.Models.Site;

namespace DCP.Migration.Models.DataHandling.BatchRequests
{
    public class EventFrameAttributesValuesRequest
    {
        public string EventFrameWebId { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
