﻿using DCP.Migration.Models.Site;

namespace DCP.Migration.Models.DataHandling.GlobalFilter
{
    public class ElementMetaDataRequest
    {
        public string ElementWebId { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
