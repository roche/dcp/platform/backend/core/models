﻿using DCP.Migration.Models.Site;

namespace DCP.Migration.Models.DataHandling.GlobalFilter
{
    public class GlobalFilterRequest
    {
        public string Link { get; set; }
        public string WebId { get; set; }
        public SiteConfigurationDTO SiteConfiguration { get; set; }
    }
}
