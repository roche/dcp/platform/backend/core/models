﻿using DCP.Migration.Models.Site;
using System.Collections.Generic;

namespace DCP.Migration.Models.Notification
{
    public class RecordedDataRequest
    {
        public List<string> SensorsWebIds { get; set; }

        public SiteConfigurationDTO SiteConfiguration { get; set; }

        public string StartTime { get; set; }

        public string EndTime { get; set; }

        public string MaxCount { get; set; }

        public string FilterExpression { get; set; }
        public bool? IncludeFilteredValues { get; set; }
    }
}
