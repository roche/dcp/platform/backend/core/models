﻿namespace DCP.Migration.Models.VaultSharp
{
    public class VaultSharpConfigurationDTO
    {
        public string VaultServerUri { get; set; }
        public string VaultNamespace { get; set; }
        public string VaultMountPoint { get; set; }
        public string VaultDataSourcePath { get; set; }
        public string MachineName { get; set; }
        public string MachineNameNonGxp { get; set; }
        public string EncryptKey { get; set; }
    }
}
