﻿using DCP.Migration.Models.AnalyticalDataEntities;
using System.Collections.Generic;

namespace DCP.Migration.Models.AnalyticalDataResponses
{
    public class AvailableAnalyticalDataAttributeDataSourceResponse
    {
        public List<AnalyticalDataAttributeDataSourceDTO> AnalyticalDataAttributes { get; set; }
        public string ErrorMessage { get; set; }
        /// <summary>
        /// Check WarningStatusCodes enum
        /// </summary>
        public int WarningCode { get; set; }
    }
}
