﻿using DCP.Migration.Models.AnalyticalDataEntities;
using System.Collections.Generic;

namespace DCP.Migration.Models.AnalyticalDataResponses
{
    public class AvailableAnalyticalDataAttributeResponse
    {
        public List<AnalyticalDataAttributeDTO> AnalyticalDataAttributes { get; set; }
        public string ErrorMessage { get; set; }
        /// <summary>
        /// Check WarningStatusCodes enum
        /// </summary>
        public int WarningCode { get; set; }
    }
}
