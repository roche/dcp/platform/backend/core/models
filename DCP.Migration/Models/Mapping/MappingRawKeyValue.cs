﻿namespace DCP.Migration.Models.Mapping
{
    public class MappingRawKeyValue
    {
        public string Name { get; set; }
        public string Value { get; set; }

    }
}