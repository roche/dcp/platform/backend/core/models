﻿using DCP.Migration.Models.ConnectionBridge;
using System.Collections.Generic;
using System.Linq;

namespace DCP.Migration.Models.Mapping
{
    public class MappingExpression
    {
        public List<MappingKey> PrimaryMappingKeys { get; private set; }
        public List<MappingKey> SecondaryMappingKeys { get; private set; }
        public List<ConnectionBridgeKeyMappingDTO> Definition { get; private set; }

        public MappingExpression(List<ConnectionBridgeKeyMappingDTO> definition)
        {
            Definition = definition;
            PrimaryMappingKeys = new List<MappingKey>();
            SecondaryMappingKeys = new List<MappingKey>();

            foreach (var mapKey in definition)
            {
                PrimaryMappingKeys.Add(new MappingKey(mapKey.PrimaryDataSourceKey));
                SecondaryMappingKeys.Add(new MappingKey(mapKey.SecondaryDataSourceKey));
            }
        }

        public bool Filter(List<MappingRawKeyValue> values)
        {
            return SecondaryMappingKeys.All(x => x.Filter(values));
        }

    }
}
