﻿using System.Collections.Generic;

namespace DCP.Migration.Models.DataSource
{
    public class EventFrameMapping
    {
        public string PrimaryGlobalIdentifierKey { get; set; }
        public List<EventFrameMappingKey> Keys { get; set; }
    }
}