﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;


namespace DCP.Migration.Models.DataSource
{
    public class DataSourceDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        /// <summary>
        /// OSISoftPI = 1, GCP = 2, SynTQ = 3, SQL = 4
        /// </summary>
        public byte Type { get; set; }
        public JObject ConnectionString { get; set; }
        public int SiteID { get; set; }
        public bool IsValidated { get; set; }
        public List<byte> SupportedDataTypes { get; set; }
    }
}
