﻿using System.Collections.Generic;

namespace DCP.Migration.Models.DataSource
{
    public class DataSourcesDetailsRequest
    {
        public List<int> DatasourceIds { get; set; }
        public int SiteId { get; set; }
    }
}
