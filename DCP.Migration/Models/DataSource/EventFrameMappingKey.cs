﻿namespace DCP.Migration.Models.DataSource
{
    public class EventFrameMappingKey
    {
        public string Name { get; set; }
        public string Value { get; set; }
    }
}