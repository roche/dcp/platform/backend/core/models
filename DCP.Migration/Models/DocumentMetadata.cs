﻿using System;

namespace DCP.Migration.Models
{
    public class DocumentMetadata
    {
        /// <summary>
        /// DocumentStatus.cs
        /// </summary>
        public byte Status { get; set; }
        public DateTime EffectiveDate { get; set; }
    }
}
