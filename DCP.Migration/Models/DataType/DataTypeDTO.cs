﻿namespace DCP.Migration.Models.DataType
{
    public class DataTypeDTO
    {
        public int DataTypeId { get; set; }
        /// <summary>
        /// EquipmentData = 1, ExperimentData = 2, ProcessMonitoringData = 3
        /// </summary>
        public byte DataType { get; set; }
        public int DataSourceId { get; set; }
    }
}
