﻿using Newtonsoft.Json.Linq;

namespace DCP.Migration.Models.OsiSoftPI
{
    public class PIFilterDTO
    {
        public string Name { get; set; }
        public string Description { get; set; }
        public string WebId { get; set; }
        public string Link { get; set; }
        public string UoM { get; set; }
        public bool IsDevice { get; set; }
        public string ValueType { get; set; }
        public bool IsChromUnit { get; set; }
        public JToken CategoryNames { get; set; }
        public bool Step { get; set; }
    }
}
