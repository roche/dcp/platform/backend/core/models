﻿using Newtonsoft.Json.Linq;

namespace DCP.Migration.Models.OsiSoftPI
{
    public class OsiPIResponse
    {
        public bool IsSuccessStatusCode { get; set; }
        public JObject Response { get; set; }
        public byte WarningCode { get; set; }
    }
}
