﻿using System.Collections.Generic;

namespace DCP.Migration.Models.OsiSoftPI
{
    public class PIFilterDTOExtended
    {
        public List<PIFilterDTO> PIFilterDTO { get; set; }
        public bool HasChildren { get; set; }
    }
}
