﻿namespace DCP.Migration.Models.OsiSoftPI
{
    public class PIFilter
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string Interval { get; set; }
    }
}
