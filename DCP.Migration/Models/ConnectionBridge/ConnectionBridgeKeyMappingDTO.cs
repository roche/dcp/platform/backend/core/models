﻿namespace DCP.Migration.Models.ConnectionBridge
{
    public class ConnectionBridgeKeyMappingDTO
    {
        public string PrimaryDataSourceKey { get; set; }
        public string SecondaryDataSourceKey { get; set; }
    }
}
