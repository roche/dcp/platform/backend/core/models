﻿using System.Collections.Generic;
namespace DCP.Migration.Models.ConnectionBridge
{
    public class ConnectionBridgeDTO
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public byte PrimaryConnectionDataType { get; set; }
        public int PrimaryConnectionDataSourcesId { get; set; }
        public byte SecondaryConnectionDataType { get; set; }
        public int SecondaryConnectionDataSourcesId { get; set; }
        public int SiteId { get; set; }
        public List<ConnectionBridgeKeyMappingDTO> KeyMapping { get; set; }
        public string Reason { get; set; }
    }
}
