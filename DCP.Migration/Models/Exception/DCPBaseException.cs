﻿namespace DCP.Migration.Models.Exception
{
    public class DCPBaseException : System.Exception
    {
        public DCPBaseException() : base()
        {
        }
        public DCPBaseException(string message) : base(message)
        {
        }
        public DCPBaseException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}
