﻿namespace DCP.Migration.Models.Exception
{
    public class DCPBadRequestException : DCPBaseException
    {
        public DCPBadRequestException() : base()
        {
        }
        public DCPBadRequestException(string message) : base(message)
        {
        }
        public DCPBadRequestException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}
