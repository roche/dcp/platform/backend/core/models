﻿namespace DCP.Migration.Models.Exception
{
    public class DCPNotFoundException : DCPBaseException
    {
        public DCPNotFoundException() : base()
        {
        }
        public DCPNotFoundException(string message) : base(message)
        {
        }
        public DCPNotFoundException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}
