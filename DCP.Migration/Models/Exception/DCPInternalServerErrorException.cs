﻿namespace DCP.Migration.Models.Exception
{
    public class DCPInternalServerErrorException : DCPBaseException
    {
        public DCPInternalServerErrorException() : base()
        {
        }
        public DCPInternalServerErrorException(string message) : base(message)
        {
        }
        public DCPInternalServerErrorException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}
