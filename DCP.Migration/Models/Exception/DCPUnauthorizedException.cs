﻿namespace DCP.Migration.Models.Exception
{
    public class DCPUnauthorizedException : DCPBaseException
    {
        public DCPUnauthorizedException() : base()
        {
        }
        public DCPUnauthorizedException(string message) : base(message)
        {
        }
        public DCPUnauthorizedException(string message, System.Exception inner) : base(message, inner)
        {
        }
    }
}
