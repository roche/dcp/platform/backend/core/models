﻿using System.Collections.Generic;

namespace DCP.Migration.Models.AnalyticalDataEntities
{
    public class AnalyticalDataDataSourceDTO
    {
        public string AttributeId { get; set; }
        public string AttributeName { get; set; }
        public List<AnalyticalDataValueDataSourceDTO> Values { get; set; }
        public byte WarningCode { get; set; }
    }
}
