﻿namespace DCP.Migration.Models.AnalyticalDataEntities
{
    public class AnalyticalDataAttributeValueDTO
    {

        public string ObsId { get; set; }
        /// <summary>
        /// also known as EventFrameId
        /// </summary>
        public string Id { get; set; }
        public string AttributeId { get; set; }
        public double? Value { get; set; }
        public string Timestamp { get; set; }
    }
}
