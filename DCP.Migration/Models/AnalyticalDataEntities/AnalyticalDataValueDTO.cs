﻿namespace DCP.Migration.Models.AnalyticalDataEntities
{
    public class AnalyticalDataValueDTO
    {
        public string ObsId { get; set; }
        /// <summary>
        /// also known as EventFrameId
        /// </summary>
        public string Id { get; set; }
        public double? Value { get; set; }
        public string Timestamp { get; set; }
    }
}
