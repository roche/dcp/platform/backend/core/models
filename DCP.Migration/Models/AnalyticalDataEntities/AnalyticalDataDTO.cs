﻿using System.Collections.Generic;

namespace DCP.Migration.Models.AnalyticalDataEntities
{
    public class AnalyticalDataDTO
    {
        public string AttributeName { get; set; }
        public string AttributeId { get; set; }
        public List<AnalyticalDataValueDTO> Values { get; set; }
        public byte WarningCode { get; set; }
    }
}
