﻿namespace DCP.Migration.Models.AnalyticalDataEntities
{
    public class AnalyticalDataAttributeDataSourceDTO
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string UoM { get; set; }
        public string Description { get; set; }
    }
}