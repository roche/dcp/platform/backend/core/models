﻿namespace DCP.Migration.Constants
{
    public enum MappingKeyOutputExpressionType : byte
    {
        SQL = 1,
        REST = 2
    }
}
