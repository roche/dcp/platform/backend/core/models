﻿using DCP.Migration.Models.DataHandling.BatchEntities;
using DCP.Migration.Models.DataHandling.BatchRequests;
using DCP.Migration.Models.DataHandling.BatchResponses;
using DCP.Migration.Models.DataHandling.DatasourceUnits;
using DCP.Migration.Models.DataHandling.GlobalFilter;
using DCP.Migration.Models.DataHandling.Sensors;
using DCP.Migration.Models.DataSource;
using DCP.Migration.Models.Mapping;
using DCP.Migration.Models.Notification;
using DCP.Migration.Models.OsiSoftPI;
using DCP.Migration.Models.Site;
using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DCP.Migration.Interfaces
{
    public interface IEquipmentDataSource : IDataSource
    {
        Task<OsiPIResponse> RecordedDataAsync(RecordedDataRequest requestModel);
        Task<OsiPIResponse> RecordedDataSynchronizedTimeStampsAsync(RecordedDataRequest requestModel);
        Task<OsiPIResponse> RecordedAtTimesDataAsync(RecordedAtTimesRequest requestModel);
        Task<JObject> GetSingleSensorDetailsAsync(SensorDetailsRequest model);
        Task<JObject> GetSingleDeviceDetailsAsync(DeviceDetailsRequest model);
        Task<OsiPIResponse> GetMultipleSensorsDetailsAsync(MultipleSensorDetailsRequest model);
        Task<OsiPIResponse> EventFrameAttributesValuesAsync(EventFrameAttributesValuesRequest model);
        Task<OsiPIResponse> EventFrameEndTimeAsync(EventFrameEndTimeRequest model);
        Task<EventFramesInProgressResponse> EventFramesInProgressAsync(EventFramesInProgressRequest model);
        Task<JArray> DataSourceUnitsAsync(DatasourceUnitsRequest model);
        Task<JArray> GetDeviceEventFrameAttributeAsync(DeviceEventFrameAttributeRequest model);
        Task<JObject> GlobalFilterBackwardsAsync(GlobalFilterRequest model);
        Task<JArray> BuildGlobalFilterFromWebIdAsync(GlobalFilterRequest model);
        Task<PIFilterDTOExtended> GetGlobalFilterAsync(GlobalFilterRequest model);
        Task<JArray> ElementMetaDataAsync(ElementMetaDataRequest model);
        Task<BatchMetadataInformation> GetDeviceEventFramesAsync(DeviceEventFramesRequest model);
        Task<BatchMetadataInformation> GetDevicesEventFramesAsync(DevicesEventFramesRequest model);
        Task<IEnumerable<SensorIdentifierDTO>> GetDeviceSensorsByGroupAsync(DeviceSensorsRequest model);
        Task<List<SensorsCategoryItemsDTO>> GetGroupedDeviceSensorsAsync(DeviceSensorsRequest model);
        Task<List<BatchContextDTO>> InterpolatedDataWithBatchContextAsync(RequestSensorData model);
        Task<List<BatchContextDTO>> PlotDataWithBatchContextAsync(PlotDataWithBatchContextRequest model);
        Task<ShowSignalsResult> InterpolatedDataAtMaturityAsync(ShowSignalsRequest model);
        Task<JArray> GetActiveBatchesPerDeviceAsync(ActiveBatchRequest model);
        Task<ShowSignalsResult> InterpolatedDataAsync(ShowSignalsRequest model);
        Task<PlotDataResponse> PlotDataAsync(PlotDataRequest model);
        Task<OsiPIResponse> InterpolatedAtTimeDataAsync(InterpolatedAtTimeRequest model);
        Task<EventFramesAttributeValuesDataSourceResponse> EventFramesAttributeValuesAsync(EventFramesAttributeValuesRequest model);
        Task<List<EventFrameMapping>> GetMappingKeyValuesAsync(List<string> eventFrameIds, MappingExpression mapping, SiteConfigurationDTO siteConfigurationDto);
    }
}