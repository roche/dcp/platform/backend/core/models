﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DCP.Migration.Interfaces
{
    public interface IDataSource
    {
        Task ValidateConnectionString();
        JObject RemoveSecret();
        string DisplayConnectionString();
        void UpdateConnectionString(JObject connectionString);
        Task<string> GetEncryptedConnectionStringAsync();
        Task SaveSecretToVault(int dataSourceId);
        List<byte> GetImplementedDataTypes();
    }
}