﻿namespace DCP.Migration.Interfaces
{
    public interface IMockDataSource : IAnalyticalDataDataSource, IEDMSDataSource, IExperimentDataSource, IEquipmentDataSource, IDataSource
    {
    }
}
