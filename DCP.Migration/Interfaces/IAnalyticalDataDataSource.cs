﻿using DCP.Migration.Models.AnalyticalDataEntities;
using DCP.Migration.Models.AnalyticalDataResponses;
using DCP.Migration.Models.DataSource;
using DCP.Migration.Models.Mapping;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace DCP.Migration.Interfaces
{
    public interface IAnalyticalDataDataSource : IDataSource
    {
        Task<AvailableAnalyticalDataAttributeDataSourceResponse> GetAvailableAnalyticalDataAttributesAsync(string deviceWebId);
        Task<List<AnalyticalDataDataSourceDTO>> GetAnalyticalDataByAttributeIdsAsync(List<EventFrameMapping> attributeIdMappings, List<string> attributes, MappingExpression mappingExpression);
    }
}
