# Security Policy

## Reporting a Vulnerability

At open-dcp.ai, we take security vulnerabilities seriously. If you discover a security issue within our project, please let us know immediately by using our [Contact Form](https://open-dcp.ai/contact) or by emailing us at [global.dcp-support@roche.com](mailto:global.dcp-support@roche.com). We appreciate your responsible disclosure.

Our team will investigate and respond to your report promptly. We will also keep you informed of our progress in addressing the issue and coordinating the release of fixes.

## Responsible Disclosure

We encourage responsible disclosure of security vulnerabilities. We ask that you:

- Do not disclose the vulnerability publicly until we have had sufficient time to address it.
- Do not exploit the vulnerability for any malicious purpose.
- Provide us with reasonable time to fix the issue before disclosing it to others.

We are committed to working with security researchers and contributors to verify, address, and resolve reported vulnerabilities in a timely manner.

## Security Releases

Upon identifying and addressing a security vulnerability, we will release a new version of our project containing the necessary fixes. We will provide details about the vulnerability and the fixes included in the release notes.

## Preferred Languages

We prefer all communications to be in English.

Thank you for helping us keep our project secure!
